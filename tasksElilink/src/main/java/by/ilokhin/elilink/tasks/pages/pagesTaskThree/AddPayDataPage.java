package by.ilokhin.elilink.tasks.pages.pagesTaskThree;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import by.ilokhin.elilink.tasks.pages.BasePage;

public class AddPayDataPage extends BasePage {
	
	@FindBy(xpath = ".//*[@id='notLogInAccNumber']")
	private WebElement fieldNcard;
	
	@FindBy(xpath = ".//*[@id='notLogInExpMonth-button']/span[1]")
	private WebElement buttonMM;
	
	@FindBy(xpath = "//li[text()='02']")
	private WebElement prefixMM;
	
	@FindBy(xpath = "//li[text()='22']")
	private WebElement prefixYY;
					
	@FindBy(xpath = ".//*[@id='notLogInExpYear-button']/span[1]")
	private WebElement buttonYY;
	
	@FindBy(xpath = ".//*[@id='notLogInName']")
	private WebElement fieldNameCard;
	
	@FindBy(xpath = ".//*[@id='addr1']")
	private WebElement fieldAddressCard;
	
	@FindBy(xpath = ".//*[@id='cityCountyWard']")
	private WebElement fieldCityCard;
	
	@FindBy(css = "#stateProv-button > span.ui-icon.ui-icon-triangle-1-s")
	private WebElement buttonStateCard;
	
	@FindBy(xpath = "//li[text()='AA']")
	private WebElement prefixStateCard;
	
	@FindBy(xpath = ".//*[@id='postal']")
	private WebElement fieldZipCard;
	
	@FindBy(xpath = ".//*[@id='stateProv-button']/span[1]")
	private WebElement buttonComplete;
	
	@FindBy(xpath = ".//*[@id='bookingModuleBody']/div[2]/div/div/div[1]/div/div[2]/fieldset/div/div[3]/label")
	private WebElement buttonNo;
	
	
	public AddPayDataPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	public AddPayDataPage chooseTicketAndSumm(String numberCard, String nameCard, String addressCard, String cityCard,
			String zipCode) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(webDriver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(buttonNo));
		buttonNo.click();
		fieldZipCard.clear();
		fieldZipCard.sendKeys(zipCode);
		Thread.sleep(500);
		buttonStateCard.click();
		wait.until(ExpectedConditions.elementToBeClickable(prefixStateCard));	
		WebElement inputSt = prefixStateCard;
		new Actions(webDriver).moveToElement(inputSt).click().perform();
		fieldNcard.clear();
		fieldNcard.sendKeys(numberCard);
		buttonMM.click();
		wait.until(ExpectedConditions.elementToBeClickable(prefixMM));
		WebElement inputMM = prefixMM;
		new Actions(webDriver).moveToElement(inputMM).click().perform();
		buttonYY.click();
		wait.until(ExpectedConditions.elementToBeClickable(prefixYY));
		WebElement inputYY = prefixYY;
		new Actions(webDriver).moveToElement(inputYY).click().perform();
		fieldNameCard.clear();
		fieldNameCard.sendKeys(nameCard);
		fieldAddressCard.clear();
		fieldAddressCard.sendKeys(addressCard);
		fieldCityCard.clear();
		fieldCityCard.sendKeys(cityCard);
		buttonComplete.isEnabled();
		

		return new AddPayDataPage(webDriver);
	}

}
