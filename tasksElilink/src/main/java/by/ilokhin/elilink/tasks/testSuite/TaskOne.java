package by.ilokhin.elilink.tasks.testSuite;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import by.ilokhin.elilink.tasks.BaseTest;
import by.ilokhin.elilink.tasks.helpers.MethodSearchForTaskOne;
import by.ilokhin.elilink.tasks.pages.pagesTaskOne.MainPage;



public class TaskOne extends BaseTest {
	private static final Logger log = Logger.getLogger(TaskOne.class.getName());


	@Test
	public void taskOne() throws InterruptedException {
		log.info("Start test - (task1)");
		driver.get("https://www.tut.by/");
		final MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
		final int count = mainPage.search("automated testing").getResultsCount();
		Assert.assertEquals(count, 16, "Incorrect results count");
		log.info("Results searching this page - " + count + " - (task1)");
		MethodSearchForTaskOne.searchTaskOne(driver);
		log.info("TEST PASSED - (task1)");

	}
}
