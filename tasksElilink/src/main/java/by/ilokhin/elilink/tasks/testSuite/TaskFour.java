package by.ilokhin.elilink.tasks.testSuite;

import by.ilokhin.elilink.tasks.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

public class TaskFour {

	public static final String TEST_DIR_NAME = "/myDir";
	private static final Logger log = Logger.getLogger(TaskFour.class.getName());

	private FTPClient client;

	@BeforeClass
	public void prepare() throws IOException {
		log.info("Start test - (task4)");
		client = new FTPClient();

		client.connect("ftp.byfly.by", 21);
	}

	@Test
	public void walkOverDirs() throws IOException {
		for (String file : client.list()) {
			final String filename = parseFileName(file);
			//not the best validation of whether it's file or dir. Would be better to completely parse PWR response
			if (!filename.contains(".")) {
				if (client.cwd(filename)) {
					//return back
					client.cwd("/");

				}
			}

		}
	}

	@AfterClass
	public void close() throws IOException {
		if (client != null) {
			client.disconnect();
		}
	}

	@Test(expectedExceptions = IllegalAccessException.class)
	public void createDir() throws Exception {
		client.createDirectory(TEST_DIR_NAME);
	}

	@Test(expectedExceptions = IllegalAccessException.class)
	public void removeDir() throws Exception {
		client.removeDirectory("/myDir");
	}

	private String parseFileName(String line) {
		return line.substring(line.lastIndexOf(' ') + 1, line.length());
	}

}