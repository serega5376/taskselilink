package by.ilokhin.elilink.tasks.helpers;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;


public class MethodSearchForTaskOne {

	private static final Logger log = Logger.getLogger(MethodSearchForTaskOne.class.getName());

	public static void searchTaskOne(WebDriver driver) throws InterruptedException {

	int count = driver.findElements(By.xpath("//li[@class='b-results__li']")).size();

	try {
		ArrayList<String> foundPages = new ArrayList<String>();
		for (int i = 0; i < count; i++) {
			int j = i;
			if (j >= 8) {
				j += 1;
			}
			foundPages.add(i,
					driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/ol/li[" + (j + 1) + "]"))
							.getText());
			if (foundPages.get(i).contains("Minsk Automated Testing Community")) {
				driver.findElement(
						By.xpath("html/body/div[3]/div/div[2]/div[2]/div/div[2]/ol/li[" + (j + 1) + "]/h3/a[2]"))
						.click();
				log.info("Is found element with" + " - " + "'" + "Minsk Automated Testing Community" + "'"
						+ " - (task1)");
				log.info("Open link....please wait!!! - (task1)");
			} 
		}
		} catch (WebDriverException e) {
			log.info("That something bad happened - (task1)");
		}
	}
}
