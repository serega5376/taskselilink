package by.ilokhin.elilink.tasks.pages.pagesTaskTwo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import by.ilokhin.elilink.tasks.pages.BasePage;

public class MainPage extends BasePage {

	@FindBy(id = "gbqfq")
	private WebElement fieldSearchGmail;

	@FindBy(xpath = "//a[contains(@title,'Входящие')]")
	private WebElement buttonInbox;

	@FindBy(xpath = "//a[contains(@title,'Отправленные')]")
	private WebElement buttonSent;

	@FindBy(css = "span.CJ")
	private WebElement buttonMore;

	@FindBy(xpath = "//a[contains(@title,'Спам')]")
	private WebElement buttonSpam;

	@FindBy(xpath = ".//*[@id=':2l']")
	private WebElement inboxHeader;
	
	@FindBy(xpath = "//button[contains(@aria-label,'Поиск Gmail')]")
	private WebElement buttonSearchGmail;
	
	@FindBy(xpath = "//div[text()='НАПИСАТЬ']")
	private WebElement buttonWrite;
	
	@FindBy(xpath = "//span[@class='gb_db gbii']")
	private WebElement buttonAccount;
	
	@FindBy(xpath = "//a[text()='Выйти']")
	private WebElement buttonExit;
	
	private static final By FIELD_LETTERS = By.xpath(".//*[@id=':2']/div/div/div[6]/div/div/div[2]/div/table/tbody/tr");
	
	private static final By FIELD_LETTERS_AFTER_SEARCH = By.xpath(".//*[@id=':2']/div/div/div[4]/div[1]/div/table/tbody/tr");
	

	public MainPage(WebDriver webDriver) {
		super(webDriver);
	}

	public MainPage checkDir(String sentHeader, String spamHeader, String inboxHeaderText) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(webDriver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(buttonSent));
		buttonSent.click();
		Thread.sleep(1000);
		Assert.assertEquals(fieldSearchGmail.getAttribute("value"), sentHeader);
		buttonMore.click();
		wait.until(ExpectedConditions.elementToBeClickable(buttonSpam));
		buttonSpam.click();
		Thread.sleep(1000);
		Assert.assertEquals(fieldSearchGmail.getAttribute("value"), spamHeader);
		buttonInbox.click();
		wait.until(ExpectedConditions.elementToBeClickable(inboxHeader));
		Assert.assertEquals(inboxHeader.getText(), inboxHeaderText);

		return new MainPage(webDriver);
	}

	public int getResultsCount() {
		return this.webDriver.findElements(FIELD_LETTERS).size();
	}
	
	public int getResultsCountAfterSearch() {
		return this.webDriver.findElements(FIELD_LETTERS_AFTER_SEARCH).size();
	}
	
	public MainPage searchLetters(String elemSearch) {

		fieldSearchGmail.sendKeys("in:inbox " + elemSearch);
		buttonSearchGmail.click();

		return new MainPage(webDriver);
	}
	
	public WriteLetterPage pressWrite() {

		buttonWrite.click();

		return new WriteLetterPage(webDriver);
	}
	
	public MainPage logout() {
		
		WebDriverWait wait = new WebDriverWait(webDriver, 20);
		buttonAccount.click();
		wait.until(ExpectedConditions.elementToBeClickable(buttonExit));
		buttonExit.click();
		
		return new MainPage(webDriver);
	}

}
